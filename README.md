# MNS

## Money Viz

- school data:
    - 2021: https://mns.school.nz/wp-content/uploads/2023/05/20220610142056558.pdf

- sankey diagrams:
    - https://github.com/d3/d3-sankey
    - https://observablehq.com/@d3/sankey
    - https://observablehq.com/@mbostock/flow-o-matic

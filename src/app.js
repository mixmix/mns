/* eslint-disable comma-dangle */

import { format as d3Format } from 'd3'
import SankeyChart from './sankey-chart'

// parcel HMR opt in
// if (module.hot) module.hot.accept()

const SCHOOL = 'School'
const GOVT = 'Governement Grants'
const LOCAL = 'Locally raised funds'
const LEARNING = 'Learning Resources'

const data = {
  links: [
    // Revenue
    // - Govt Grants
    { source: GOVT, target: SCHOOL, value: 2410853 },
    { source: 'Operational Grants', target: GOVT, value: 472237 },
    { source: 'Teachers Salaries', target: GOVT, value: 1427901 },
    { source: 'Use of Land and Buildings Grants', target: GOVT, value: 394023 },
    { source: 'Other MoE Grants', target: GOVT, value: 116692 },

    // - Locally raised funds
    { source: LOCAL, target: SCHOOL, value: 162397 },
    { source: 'Donations & Bequests', target: LOCAL, value: 71556 },
    { source: 'Fees for Extra Curricular Activities', target: LOCAL, value: 41398 },
    { source: 'Trading', target: LOCAL, value: 4072 },
    { source: 'Fundraising & Community Grants', target: LOCAL, value: 23706 },
    { source: 'Pool', target: LOCAL, value: 21665 },

    { source: 'Interest Income', target: SCHOOL, value: 451 },

    // Expenses
    { source: SCHOOL, target: 'Locally raised funds (expenses)', value: 22742 },

    { source: SCHOOL, target: LEARNING, value: 1814846 },
    { source: LEARNING, target: 'Curricular', value: 43733 },
    { source: LEARNING, target: 'Library Resources', value: 211 },
    { source: LEARNING, target: 'Salaries', value: 1765908 },
    { source: LEARNING, target: 'Staff Development', value: 5994 },

    { source: SCHOOL, target: 'Administration', value: 152374 },
    { source: SCHOOL, target: 'Finance', value: 6913 },
    { source: SCHOOL, target: 'Property', value: 524289 },
    { source: SCHOOL, target: 'Deprecation', value: 63762 },
    { source: SCHOOL, target: 'Loss', value: 10 },
  ]
}

const chart = SankeyChart(data, {
  width: 1000,
  height: 640,
  nodeGroup: d => d.id.split(/\W/)[0], // take first word for color
  format: (f => d => `$${f(d)}`)(d3Format(',.1~f')),
  linkColor: 'source-target'
})

document.body.appendChild(chart)
